import numpy as np
from pyspark.ml.feature import VectorAssembler
from pyspark.mllib.clustering import KMeans
from pyspark.mllib.linalg import Vectors
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, desc, udf
from pyspark.sql.types import FloatType, IntegerType

feat_cols = [
    "LBE",
    "LB",
    "AC",
    "FM",
    "UC",
    "ASTV",
    "MSTV",
    "ALTV",
    "MLTV",
    "DL",
    "DS",
    "DP",
    "DR",
    "Width",
    "Min",
    "Max",
    "Nmax",
    "Nzeros",
    "Mode",
    "Mean",
    "Median",
    "Variance",
    "Tendency",
    "A",
    "B",
    "C",
    "D",
    "E",
    "AD",
    "DE",
    "LD",
    "FS",
    "SUSP",
    "CLASS",
]

spark = SparkSession.builder.appName("Python Spark SQL basic example").getOrCreate()

df_ham = spark.read.csv("data.csv", header=True, sep=",", inferSchema=True)
df_ham = df_ham.where("NSP != 2")
df = df_ham.select(*feat_cols)


def calculate_projection_C(all_data, n_components, c_alpha):
    num_instances, num_events = all_data.shape
    all_data_cov = np.dot(all_data.T, all_data) / float(num_instances)
    U, sigma, V = np.linalg.svd(all_data_cov)

    if n_components < 1:
        total_variance = np.sum(sigma)
        variance = 0
        for i in range(num_events):
            variance += sigma[i]
            if variance / total_variance >= n_components:
                break
        n_components = i + 1

    P = U[:, :n_components]
    I = np.identity(num_events, int)

    proj_C = I - np.dot(P, P.T)

    # print("n_components: {}".format(n_components))
    # print("Project matrix shape: {}-by-{}".format(proj_C.shape[0], proj_C.shape[1]))
    #
    #
    # THRESHOLD
    # ============================
    # Calculate threshold using Q-statistic. Information can be found at:
    # http://conferences.sigcomm.org/sigcomm/2004/papers/p405-lakhina111.pdf
    phi = np.zeros(3)
    for i in range(3):
        for j in range(n_components, num_events):
            phi[i] += np.power(sigma[j], i + 1)
    h0 = 1.0 - 2 * phi[0] * phi[2] / (3.0 * phi[1] * phi[1])
    threshold = phi[0] * np.power(
        c_alpha * np.sqrt(2 * phi[1] * h0 * h0) / phi[0]
        + 1.0
        + phi[1] * h0 * (h0 - 1) / (phi[0] * phi[0]),
        1.0 / h0,
    )

    return proj_C, threshold


all_data = np.array(df.collect())

maxx = 0


for x in range(1, 30):
    n = 10
    treshold = x * 0.1  # for the get 176 1
    PROJECTION_C, THRESHOLD = calculate_projection_C(all_data, n, treshold)

    def f(*row):
        y_a = np.dot(PROJECTION_C, row)

        SPE = np.dot(y_a, y_a)

        if SPE > THRESHOLD:
            return 1
        return 0

    udfValueToCategory = udf(f, IntegerType())

    df_ham = df_ham.withColumn("predicted_PCA", udfValueToCategory(*feat_cols))

    # EVALUTATION
    def puan_hesapla(row):
        # (truePozitif, falsePozitif, trueNegativ, falseNegative)

        nsp = row["NSP"]
        predicted = row["predicted_PCA"]

        if predicted == 1 and nsp == 3:
            return (1, 0, 0, 0)
        if predicted == 1 and nsp == 1:
            return (0, 1, 0, 0)
        if predicted == 0 and nsp == 1:
            return (0, 0, 1, 0)
        if predicted == 0 and nsp == 3:
            return (0, 0, 0, 1)

    sonuc = df_ham.rdd.map(puan_hesapla).collect()

    TP = 0
    FP = 0
    TN = 0
    FN = 0
    for j in sonuc:
        TP += j[0]
        FP += j[1]
        TN += j[2]
        FN += j[3]

    pres = TP / (TP + FP)
    rec = TP / (TP + FN)
    print(
        f"th:{treshold},TP:{TP}, FP:{FP}, TN:{TN}, FN:{FN}, precision:{pres}, recall:{rec}, f-s={2*((pres*rec)/(pres+rec))}"
    )

print(1 / 0)
#############################################################################################

vectorAss = VectorAssembler(inputCols=feat_cols, outputCol="features")
df_ham = vectorAss.transform(df_ham)
data_for_kmeans = df_ham.rdd.map(
    lambda row: Vectors.dense([x for x in row["features"]])
)


k = 3
for i in range(1, 20):
    CLUSTERS = KMeans.train(
        data_for_kmeans,
        k=k,
        maxIterations=1000
        # , initializationMode="random"
    )

    def add_distance(*row):
        record = Vectors.dense(row[0])
        cluster = CLUSTERS.predict(record)
        centroid = CLUSTERS.centers[cluster]
        return float(np.linalg.norm(record - centroid))

    udfValueToCategory = udf(add_distance, FloatType())
    df_ham_tmp = df_ham.withColumn("distance_kMeans", udfValueToCategory("features"))
    THRESHOLD = df_ham_tmp.sort(desc("distance_kMeans")).take(i * 100)[-1][
        "distance_kMeans"
    ]

    def add_kMeans_label(*row):
        if row[0] > THRESHOLD:
            return 1
        return 0

    udfValueToCategory = udf(add_kMeans_label, IntegerType())
    df_ham_tmp = df_ham_tmp.withColumn(
        "label_kMeans", udfValueToCategory("distance_kMeans", "NSP")
    )

    def puan_hesapla(row):
        nsp = row["NSP"]
        predicted = row["label_kMeans"]

        if predicted == 1 and nsp == 3:
            return (1, 0, 0, 0)
        if predicted == 1 and nsp == 1:
            return (0, 1, 0, 0)
        if predicted == 0 and nsp == 1:
            return (0, 0, 1, 0)
        if predicted == 0 and nsp == 3:
            return (0, 0, 0, 1)

    sonuc = df_ham_tmp.rdd.map(puan_hesapla).collect()

    TP = 0
    FP = 0
    TN = 0
    FN = 0
    for j in sonuc:
        TP += j[0]
        FP += j[1]
        TN += j[2]
        FN += j[3]

    pres = TP / (TP + FP)
    rec = TP / (TP + FN)
    print(
        f"TP:{TP}, FP:{FP}, TN:{TN}, FN:{FN}, precision:{pres}, recall:{rec}, f-s={2*((pres*rec)/(pres+rec))}"
    )


print("*************************************************************")


####################################################################

# 1 125042.14361274328
# 2 98416.75977933269
# 3 93564.26896729406
# 4 87068.11587423657
# 5 81077.18741134949
# 6 77275.2512598728
# 7 72855.63039320617
# 8 71715.32889144972
# 9 69429.46610894697
# 10 68234.02505110591
# 11 65044.12713176603
# 12 64238.721456927495
# 13 63217.08459456499
# 14 61378.313329100594
# 15 61258.82941445935
# 16 59984.78293153298
# 17 59135.30976512104
# 18 58211.4994580777
# 19 57464.071677977416
# 20 57389.70995930132
# 21 56095.38852477147
# 22 54944.005982695075
# 23 54506.97328481606
# 24 55119.09825483055
# 25 53369.11597996757
# 26 52667.48344239421
# 27 52186.48241890176
# 28 51755.484175588506
# 29 51800.66724120141
# 30 50159.772287161955
# 31 50453.96383943524
# 32 50433.48392558571
# 33 49189.0038695005
# 34 48681.6709982666
# 35 49769.57519834522
# 36 48841.994394154855
# 37 47326.881583412964
# 38 47445.350987117345
# 39 47272.80195977793
# 40 46786.06042949167
# 41 46650.473231844575
# 42 46547.40130342587
# 43 46083.304299884774
# 44 45340.842970251906
# 45 45491.08661073764
# 46 44717.996878827864
# 47 44903.81557989613
# 48 44726.75795913601
# 49 44316.953226179816


for k in range(1, 1):
    # elbow method w
    CLUSTERS = KMeans.train(
        data_for_kmeans,
        k=k,
        maxIterations=100
        # , initializationMode="random"
    )

    def get_distance_map(row):
        record = Vectors.dense([x for x in row["features"]])

        cluster = CLUSTERS.predict(record)
        centroid = CLUSTERS.centers[cluster]
        dist = np.linalg.norm(record - centroid)
        return dist

    data_distance = df_ham.rdd.map(get_distance_map).reduce(lambda x, y: x + y)
    print(k, data_distance)
