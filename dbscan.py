import numpy as np
from pyspark.ml import Pipeline
from pyspark.ml.feature import MinMaxScaler, VectorAssembler
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.types import DoubleType
from sklearn.cluster import DBSCAN
from sklearn.neighbors import NearestNeighbors

import matplotlib.pyplot as plt

feat_cols = [
    "LBE",
    "LB",
    "AC",
    "FM",
    "UC",
    "ASTV",
    "MSTV",
    "ALTV",
    "MLTV",
    "DL",
    "DS",
    "DP",
    "DR",
    "Width",
    "Min",
    "Max",
    "Nmax",
    "Nzeros",
    "Mode",
    "Mean",
    "Median",
    "Variance",
]

spark = SparkSession.builder.appName("Python Spark SQL basic example").getOrCreate()

df_ham = spark.read.csv("data_normalized.csv", header=True, sep=",", inferSchema=True)
df_ham = df_ham.where("NSP != 2")
df = df_ham.select(*feat_cols)

#####################################################################################

X_ham = df_ham.toPandas()  # for evaluation
X = df.toPandas()


def find_epsilon():
    neigh = NearestNeighbors(n_neighbors=2)
    nbrs = neigh.fit(X)
    distances, indices = nbrs.kneighbors(X)

    distances = np.sort(distances, axis=0)
    distances = distances[:, 1]
    plt.plot(distances)
    plt.show()  # NOTE ipythonda çalışmalı


def print_statistic(labels):
    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)

    print(" => # clusters: %d " % n_clusters_, end="")
    print("# noise points: %d" % n_noise_)


def evaluate(labels, X):
    X["predicted_dbscan"] = labels

    TP = 0
    FP = 0
    TN = 0
    FN = 0

    for index, row in X.iterrows():

        nsp = row["NSP"]
        predicted = 0
        if row["predicted_dbscan"] == -1:
            predicted = 1

        if predicted == 1 and nsp == 3:
            TP += 1
        if predicted == 1 and nsp == 1:
            FP += 1
        if predicted == 0 and nsp == 1:
            TN += 1
        if predicted == 0 and nsp == 3:
            FN += 1

    if (TP + FP) == 0 or (TP + FN) == 0:
        print("hata")
        return
    pres = TP / (TP + FP)
    rec = TP / (TP + FN)
    print(
        f"TP:{TP}, FP:{FP}, TN:{TN}, FN:{FN}, precision:{pres}, recall:{rec}, f-s={2*((pres*rec)/(pres+rec))}"
    )


eps = 0.40
min_samples = 15

db = DBSCAN(eps=eps, min_samples=min_samples).fit(X)
labels = db.labels_
print_statistic(labels)
print("eps:", eps, "min_samples", min_samples, "sonuç:")
evaluate(labels, X_ham)
print()
