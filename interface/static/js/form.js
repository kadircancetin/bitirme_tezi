let $P1 = $("#paramter1-container");
let $P2 = $("#paramter2-container");
let SELECTED_ALGORITHM = $("#algorithm").val();

let ALGORITHM_PARAMTER_VALUES_MAP = {
  pca : {
    p1: {
      name: "Threshold (c_alpha)",
      default_value: -1.282
    },
    p2: {
      name: "N",
      default_value: 2,
    }
  },
  dbscan: {
    p1: {
      name: "Epsilon",
      default_value: 0.4
    },
    p2: {
      name: "Min Samples",
      default_value: 15,
    }
  },
  kmeans: {
    p1: {
      name: "K",
      default_value: 3
    },
    p2: {
      name: "Threshold (%)",
      default_value: 20,
    }    
  },
}


function change_var_names(){
  SELECTED_ALGORITHM = $("#algorithm").val();
  let tmp = ALGORITHM_PARAMTER_VALUES_MAP[SELECTED_ALGORITHM];

  $P1.find("label").html(tmp.p1.name);
  $P1.find("input").attr("placeholder", tmp.p1.name);
  $P1.find("input").val(tmp.p1.default_value);
  
  $P2.find("label").html(tmp.p2.name);
  $P2.find("input").attr("placeholder", tmp.p2.name);
  $P2.find("input").val(tmp.p2.default_value);
}

change_var_names();

$("#algorithm").change(()=>{
  change_var_names();
})


