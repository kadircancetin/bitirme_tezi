var LINE_COLORS = [
  "#69B3A2",
  "#9CE083",
  "#718EB6",
  "#FFD796",
  "#FFBE96",
  "#718EB6",
]

var WILL_DRAW_VALUES = [
  "LBE",
  "LB",
  "AC",
  "FM",
  "UC",
  "ASTV",
  "MSTV",
  "ALTV",
  "MLTV",
  "DL",
  "DS",
  "DP",
  "DR",
  "Width",
  "Min",
  "Max",
  "Nmax",
  "Nzeros",
  "Mode",
  "Mean",
  "Median",
  "Variance"
]

var WILL_SHOW_VALUES = [
  "Re-login Count",
  "ClickCount",
  "DeviceCategory",
  "PageCount",
  "AverageClickCount",
  "LiveTimeValue",
  "BounceRate",
  "CompanyType",
  "CountryType",
  "FraudUser",
  "EventCount",
  "AverageSessionEvent",
  "SessionDuration",
  "BrowserType",
  "DomainType",
  "NonBounceEvent",
  "SessionDurationperHour",
  "SuccessEventCount",
  "PathCount",
  "CustomerSegment",
  "SessionID",
  "UniqueSeesionID",
  "CustomerType",
]

WILL_DRAW_VALUES.forEach(function(val, i){
  
  $("#feature").append(new Option(WILL_SHOW_VALUES[i], val));
});


function default_parse(data, control_list){
  ret = [];
  data.lines.forEach(datum =>{
    if (! control_list.includes(datum.id)){
      return;
    }
    
    tmp = {
      id: datum.id,
      date: d3.timeParse("%Y-%m-%dT%H:%M:%S")(datum.date),
    };

    WILL_DRAW_VALUES.forEach((value_name)=>{
      tmp = {
        ...tmp,
        [value_name]:datum.values[value_name]
      }
    });
    ret.push(tmp);

  });
  return ret;
}
function parse_json_lines(data){
  ids = []
  data.lines.forEach(x=>{
    ids.push(x.id);
  });
  ret = default_parse(data,ids);
  return ret;  
}


function parse_json_anomalies(data){
  // data = data.lines;
  ret = default_parse(data, data.anomalies);
  return ret;  
}

function parse_false_negatives(data){
  ret = default_parse(data, data.false_negatives);
  return ret;  
}

function parse_false_positives(data){
  ret = default_parse(data, data.false_positives);
  return ret;  
}


function draw_graph(column){
  // $("#my_dataviz").html(""); // TODO: tek graph
  // set the dimensions and margins of the graph


  var margin = {top: 10, right: 30, bottom: 30, left: 60},
      width = parseInt($("#svg-container").css("width"))*(9/12) - margin.left - margin.right,
      height = 400 - margin.top - margin.bottom;

  // append the svg object to the body of the page
  
  var svg = d3.select("#my_dataviz")
      .insert("div", "div")
      .attr("class", "row")  
      .insert("div", "div")
      .attr("class", "col-9")
      .attr("id","naber")
      .insert("svg", "svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

  $("#my_dataviz").prepend("<div id='loading'>CALCULATING...</div>");  

  d3.json("/veri/",
          function(data) {
            $("#loading").remove();
            data_lines = parse_json_lines(data);
            data_anomalies = parse_json_anomalies(data);
            data_false_negatives = parse_false_negatives(data);
            data_false_positives = parse_false_positives(data);
            // Add Y axis
            var x = d3.scaleLinear().domain( [0, data_lines.length]).range([ 0, width ]);
            svg.append("g").attr("transform", "translate(0," + height + ")").attr("class", "axis-white")
              .call(d3.axisBottom(x));

            // Add Y axis
            var y = d3.scaleLinear().domain( [0, 1]).range([ height, 0 ]);
            svg.append("g").attr("class", "axis-white").call(d3.axisLeft(y));

            [column].forEach((value_name, i) => {
              // Add the line
              svg.append("path").datum(data_lines).attr("fill", "none").attr("stroke", LINE_COLORS[i])
                .attr("stroke-width", 1.5).attr(
                  "d", d3.line().x(function(d) { return x(d.id) }).y(function(d) { return y(d[value_name]) }))

              // Add the points
              svg.append("g").selectAll("dot").data(data_anomalies).enter()
                .append("circle").attr("class", "myCircle").attr("cx", function(d) { return x(d.id) } )
                .attr("cy", function(d) { return y(d[value_name]) } ).attr("r", 3).attr("stroke", "red")
                .attr("stroke-width", 3).attr("fill", "red")
              // Add the points
              svg.append("g").selectAll("dot").data(data_false_positives).enter().append("circle")
                .attr("class", "myCircle").attr("cx", function(d) { return x(d.id) } )
                .attr("cy", function(d) { return y(d[value_name]) } ).attr("r", 2).attr("stroke", "blue")
                .attr("stroke-width", 3).attr("fill", "blue")
              // Add the points
              svg.append("g").selectAll("dot").data(data_false_negatives).enter().append("circle")
                .attr("class", "myCircle").attr("cx", function(d) { return x(d.id) } )
                .attr("cy", function(d) { return y(d[value_name]) } ).attr("r", 1).attr("stroke", "yellow")
                .attr("stroke-width", 3).attr("fill", "yellow")
              
            });

            let pres = data_anomalies.length / (data_anomalies.length + data_false_positives.length)
            let rec = data_anomalies.length / (data_anomalies.length + data_false_negatives.length)

            var table = `
           <div class="row">
              <div class="col-12">
                <table class="table table-striped bg-white rounded">
                  <thead>
                    <tr>
                      <th scope="col"></th>
                      <th scope="col">True</th>
                      <th scope="col">False</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">Positive</th>
                      <td>${data_anomalies.length}</td>
                      <td>${data_false_positives.length}</td>
                    </tr>
                    <tr>
                      <th scope="row">Negative</th>
                      <td>${data_lines.length - (data_anomalies.length + data_false_positives.length + data_false_negatives.length)}</td>
                      <td>${data_false_negatives.length}</td>
                    </tr>

                  </tbody>
                </table>
              </div>
           </div>
           <div class="row rounded">
              <div class="col-12 rounded">
                <table class="table table-striped bg-white rounded">
                  <tbody>
                    <tr>
                      <td>Precision</td>
                      <td>${pres.toFixed(3)}</td>
                    </tr>
                    <tr>
                      <td>Recall</td>
                      <td>${rec.toFixed(3)}</td>
                    </tr>

                  </tbody>
                </table>
              </div>
           </div>
`;

            $("#naber").parent().append(`<div class='col-3 align-items-center d-flex flex-column justify-content-center'>${table}</div>`);

            

            
          }
         )
    .header("Content-Type","application/json")
    .send("POST",  JSON.stringify(
      {
        algorithm: $("#algorithm").val(),
        parameter_1: $("#parameter_1").val(),
        parameter_2: $("#parameter_2").val(),
      }
    ));

  console.log("mk");
}
// WILL_DRAW_VALUES.forEach((x)=>{
//   draw_graph(x);
// });

$("#cizdir_button").click(()=>{
  draw_graph($("#feature").val());
});
