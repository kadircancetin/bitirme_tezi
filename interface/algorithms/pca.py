import numpy as np
import pandas as pd
from pyspark.ml.feature import VectorAssembler
from pyspark.mllib.clustering import KMeans
from pyspark.mllib.linalg import Vectors
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, desc, udf
from pyspark.sql.types import FloatType, IntegerType

feat_cols = [
    "LBE",
    "LB",
    "AC",
    "FM",
    "UC",
    "ASTV",
    "MSTV",
    "ALTV",
    "MLTV",
    "DL",
    "DS",
    "DP",
    "DR",
    "Width",
    "Min",
    "Max",
    "Nmax",
    "Nzeros",
    "Mode",
    "Mean",
    "Median",
    "Variance",
]

spark = SparkSession.builder.appName("Python Spark SQL basic example").getOrCreate()


def calculate_projection_C(all_data, n_components, c_alpha):
    # temel alınan kaynak kod: https://github.com/logpai/loglizer/blob/d83aaa5995a31b82a749905e2622e3e7624cb8e0/loglizer/models/PCA.py#L17
    num_instances, num_events = all_data.shape
    all_data_cov = np.dot(all_data.T, all_data) / float(num_instances)
    U, sigma, V = np.linalg.svd(all_data_cov)

    if n_components < 1:
        total_variance = np.sum(sigma)
        variance = 0
        for i in range(num_events):
            variance += sigma[i]
            if variance / total_variance >= n_components:
                break
        n_components = i + 1

    P = U[:, :n_components]
    I = np.identity(num_events, int)

    proj_C = I - np.dot(P, P.T)

    # print("n_components: {}".format(n_components))
    # print("Project matrix shape: {}-by-{}".format(proj_C.shape[0], proj_C.shape[1]))
    #
    #
    # THRESHOLD
    # ============================
    # Calculate threshold using Q-statistic. Information can be found at:
    # http://conferences.sigcomm.org/sigcomm/2004/papers/p405-lakhina111.pdf
    phi = np.zeros(3)
    for i in range(3):
        for j in range(n_components, num_events):
            phi[i] += np.power(sigma[j], i + 1)
    h0 = 1.0 - 2 * phi[0] * phi[2] / (3.0 * phi[1] * phi[1])
    threshold = phi[0] * np.power(
        c_alpha * np.sqrt(2 * phi[1] * h0 * h0) / phi[0]
        + 1.0
        + phi[1] * h0 * (h0 - 1) / (phi[0] * phi[0]),
        1.0 / h0,
    )

    return proj_C, threshold


def pca(file_path, c_alpha=-0.9, n=2):
    n = int(n)
    df_ham = spark.read.csv(file_path, header=True, sep=",", inferSchema=True)
    df_ham = df_ham.where("NSP != 2")

    df = df_ham.select(*feat_cols)
    all_data = np.array(df.collect())
    PROJECTION_C, THRESHOLD = calculate_projection_C(all_data, n, c_alpha)

    def f(*row):
        y_a = np.dot(PROJECTION_C, row)
        SPE = np.dot(y_a, y_a)

        if SPE > THRESHOLD:
            return -1
        return 0

    udfValueToCategory = udf(f, IntegerType())
    df = df_ham.withColumn("predicted", udfValueToCategory(*feat_cols))

    X_pd = df.toPandas()
    X_ham = df_ham.toPandas()

    X_ham["predicted"] = X_pd["predicted"]
    X_ham["Date"] = pd.to_datetime(X_ham.Date)
    # X_ham = X_ham.sort_values(by="Date")
    X_ham["id"] = [x for x in range(len(X_ham))]
    return X_ham

    ####################################################


if __name__ == "__main__":
    print(pca("../media/2UVJ1WKFTRVQR9VNHCWE.csv"))
