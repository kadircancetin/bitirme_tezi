import numpy as np
import pandas as pd
from pyspark.ml import Pipeline
from pyspark.ml.feature import MinMaxScaler, VectorAssembler
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.types import DoubleType
from sklearn.cluster import DBSCAN
from sklearn.neighbors import NearestNeighbors

import matplotlib.pyplot as plt

spark = SparkSession.builder.appName("Python Spark SQL basic example").getOrCreate()

feat_cols = [
    "LBE",
    "LB",
    "AC",
    "FM",
    "UC",
    "ASTV",
    "MSTV",
    "ALTV",
    "MLTV",
    "DL",
    "DS",
    "DP",
    "DR",
    "Width",
    "Min",
    "Max",
    "Nmax",
    "Nzeros",
    "Mode",
    "Mean",
    "Median",
    "Variance",
]


def dbscan(file_path, eps, min_samples):
    df_ham = spark.read.csv(file_path, header=True, sep=",", inferSchema=True)
    df_ham = df_ham.where("NSP != 2")  # TODO: move it from data_preprocess
    df = df_ham.select(*feat_cols)

    #####################################################################################

    X_ham = df_ham.toPandas()  # for evaluation
    X = df.toPandas()

    def evaluate(X_ham):
        TP = 0
        FP = 0
        TN = 0
        FN = 0
        sonuclar = []

        for index, row in X_ham.iterrows():

            nsp = row["NSP"]
            predicted = 0

            if row["predicted"] == -1:
                predicted = 1

            if predicted == 1 and nsp == 3:
                TP += 1
                sonuclar.append(0)
            if predicted == 1 and nsp == 1:
                FP += 1
                sonuclar.append(1)
            if predicted == 0 and nsp == 1:
                TN += 1
                sonuclar.append(2)
            if predicted == 0 and nsp == 3:
                FN += 1
                sonuclar.append(3)

        X_ham["sonuclar"] = sonuclar
        if (TP + FP) == 0 or (TP + FN) == 0:
            # print("hata")
            return
        # pres = TP / (TP + FP)
        # rec = TP / (TP + FN)
        # print(
        #     f"TP:{TP}, FP:{FP}, TN:{TN}, FN:{FN}, precision:{pres}, recall:{rec}, f-s={2*((pres*rec)/(pres+rec))}"
        # )

        return X_ham

    db = DBSCAN(eps=eps, min_samples=min_samples).fit(X)
    labels = db.labels_
    X_ham["predicted"] = labels
    # print(labels)
    # print("eps:", eps, "min_samples", min_samples, "sonuç:")
    X_ham = evaluate(X_ham)

    X_ham["Date"] = pd.to_datetime(X_ham.Date)
    X_ham = X_ham.sort_values(by="Date")

    # X_ham = X_ham[
    #     (X_ham["Date"] > pd.to_datetime("1995-01-01"))
    #     & (X_ham["Date"] < pd.to_datetime("1995-2-01"))
    # ]

    X_ham["id"] = [x for x in range(len(X_ham))]
    return X_ham


# k = dbscan("/home/kadir/bitirme_tezi/interface/media/QZFM61TXYLX8FTV9NJPS.csv")
# print(k)
