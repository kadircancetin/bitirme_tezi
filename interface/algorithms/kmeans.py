import numpy as np
import pandas as pd
from pyspark.ml.feature import VectorAssembler
from pyspark.mllib.clustering import KMeans
from pyspark.mllib.linalg import Vectors
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, desc, udf
from pyspark.sql.types import FloatType, IntegerType

feat_cols = [
    "LBE",
    "LB",
    "AC",
    "FM",
    "UC",
    "ASTV",
    "MSTV",
    "ALTV",
    "MLTV",
    "DL",
    "DS",
    "DP",
    "DR",
    "Width",
    "Min",
    "Max",
    "Nmax",
    "Nzeros",
    "Mode",
    "Mean",
    "Median",
    "Variance",
]

spark = SparkSession.builder.appName("Python Spark SQL basic example").getOrCreate()


def kmeans(file_path, k=3, threshold=20):
    k = int(k)
    threshold = int(threshold)

    df_ham = spark.read.csv(file_path, header=True, sep=",", inferSchema=True)

    df_ham = df_ham.where("NSP != 2")
    vectorAss = VectorAssembler(inputCols=feat_cols, outputCol="features")
    df_ham = vectorAss.transform(df_ham)

    data_for_kmeans = df_ham.rdd.map(
        lambda row: Vectors.dense([x for x in row["features"]])
    )

    CLUSTERS = KMeans.train(
        data_for_kmeans,
        k=k,
        maxIterations=100
        # , initializationMode="random"
    )

    def add_distance(*row):
        record = Vectors.dense(row[0])
        cluster = CLUSTERS.predict(record)
        centroid = CLUSTERS.centers[cluster]
        return float(np.linalg.norm(record - centroid))

    udfValueToCategory = udf(add_distance, FloatType())
    df_ham_tmp = df_ham.withColumn("distance_kMeans", udfValueToCategory("features"))

    THRESHOLD = df_ham_tmp.sort(desc("distance_kMeans")).take(
        int(df_ham.count() * threshold / 100)
    )[-1]["distance_kMeans"]

    def add_kMeans_label(*row):
        if row[0] > THRESHOLD:
            return -1
        return 0

    udfValueToCategory = udf(add_kMeans_label, IntegerType())

    X_ham = df_ham_tmp.withColumn(
        "predicted", udfValueToCategory("distance_kMeans", "NSP")
    )
    X_ham = X_ham.toPandas()
    X_ham["Date"] = pd.to_datetime(X_ham.Date)
    X_ham = X_ham.sort_values(by="Date")
    X_ham["id"] = [x for x in range(len(X_ham))]

    return X_ham


if __name__ == "__main__":
    print(
        kmeans(
            "/home/kadir/bitirme_tezi/interface/media/YDVXWCD52KRBAOTITD5O.csv",
            3.0,
            20,
        )
    )
