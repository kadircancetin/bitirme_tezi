import random
import string

from django.core.files.storage import default_storage
from django.shortcuts import redirect, render
from django.views import View
from rest_framework.response import Response
from rest_framework.views import APIView

from algorithms.dbscan import dbscan, feat_cols
from algorithms.kmeans import kmeans
from algorithms.pca import pca

# Create your views here.


class home(View):
    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, "home.html", context)

    def post(self, request, *args, **kwargs):
        file_name = request.session.get("file_name")

        if file_name is None:
            file_name = (
                "".join(random.choices(string.ascii_uppercase + string.digits, k=20))
                + ".csv"
            )
            request.session["file_name"] = file_name
        else:
            default_storage.delete(file_name)
            file_name = (
                "".join(random.choices(string.ascii_uppercase + string.digits, k=20))
                + ".csv"
            )
            print("yni", file_name)
            request.session["file_name"] = file_name

        if request.FILES.get("file") is None:
            return redirect("/")

        file = request.FILES["file"]
        default_storage.save(file_name, file)
        print(request.POST, request.FILES)
        return redirect("/")


class VeriToJson(APIView):
    def post(self, request, *args, **kwargs):
        algorithm = request.data.get("algorithm")
        parameter_1 = float(request.data.get("parameter_1"))
        parameter_2 = float(request.data.get("parameter_2"))

        req_file_name = request.session.get("file_name")
        if req_file_name is not None:
            _file = default_storage.open(req_file_name)
            print(_file.name, parameter_1, parameter_2)
            # df = dbscan(_file.name)

            if algorithm == "pca":
                df = pca(_file.name, parameter_1, parameter_2)

            elif algorithm == "dbscan":
                df = dbscan(_file.name, parameter_1, parameter_2)
            elif algorithm == "kmeans":
                df = kmeans(_file.name, parameter_1, parameter_2)

            ret = {
                "lines": [],
                "anomalies": [],
                "false_negatives": [],
                "false_positives": [],
            }
            for index, row in df.iterrows():
                values = {}
                for col in feat_cols:
                    values[col] = row[col]

                ret["lines"].append(
                    {
                        "values": values,
                        "id": row["id"],
                        # "date": row["Date"]
                    }
                )

                # predicted -1 = anomali  0=normal
                # nsp = 3 anomali nsp 1=normal
                nsp_anoamli = 3
                nsp_normal = 1
                predicted_anomali = -1
                predicted_normal = 0

                if row["predicted"] == predicted_anomali and row["NSP"] == nsp_anoamli:
                    ret["anomalies"].append(index)

                if row["predicted"] == predicted_normal and row["NSP"] == nsp_anoamli:
                    ret["false_negatives"].append(index)

                if row["predicted"] == predicted_anomali and row["NSP"] == nsp_normal:
                    ret["false_positives"].append(index)

            print(
                len(ret["anomalies"]),
                len(ret["false_negatives"]),
                len(ret["false_positives"]),
            )

            return Response(ret)

        else:
            return Response(
                {
                    "lines": [
                        {
                            "id": 0,
                            "date": "2018-04-14",
                            "values": {
                                "Variance": random.random(),
                                "value2": random.random(),
                            },
                        },
                        {
                            "id": 1,
                            "date": "2018-04-15",
                            "values": {
                                "Variance": random.random(),
                                "value2": random.random(),
                            },
                        },
                        {
                            "id": 2,
                            "date": "2018-04-16",
                            "values": {
                                "Variance": random.random(),
                                "value2": random.random(),
                            },
                        },
                        {
                            "id": 3,
                            "date": "2018-04-17",
                            "values": {
                                "Variance": random.random(),
                                "value2": random.random(),
                            },
                        },
                        {
                            "id": 4,
                            "date": "2018-04-18",
                            "values": {
                                "Variance": random.random(),
                                "value2": random.random(),
                            },
                        },
                        {
                            "id": 5,
                            "date": "2018-04-19",
                            "values": {
                                "Variance": random.random(),
                                "value2": random.random(),
                            },
                        },
                        {
                            "id": 6,
                            "date": "2018-04-20",
                            "values": {
                                "Variance": random.random(),
                                "value2": random.random(),
                            },
                        },
                        {
                            "id": 7,
                            "date": "2018-04-21",
                            "values": {
                                "Variance": random.random(),
                                "value2": random.random(),
                            },
                        },
                        {
                            "id": 8,
                            "date": "2018-04-22",
                            "values": {
                                "Variance": random.random(),
                                "Variance2": random.random(),
                            },
                        },
                        {
                            "id": 9,
                            "date": "2018-04-23",
                            "values": {
                                "Variance": random.random(),
                                "value2": random.random(),
                            },
                        },
                        {
                            "id": 10,
                            "date": "2018-04-28",
                            "values": {
                                "Variance": random.random(),
                                "value2": random.random(),
                            },
                        },
                    ],
                    "anomalies": [1, 3, 7],
                }
            )
