import numpy as np
import scipy as sp
from sklearn.preprocessing import MinMaxScaler

import pandas as pd
from pandas import read_csv

feat_cols = [
    "LBE",
    "LB",
    "AC",
    "FM",
    "UC",
    "ASTV",
    "MSTV",
    "ALTV",
    "MLTV",
    "DL",
    "DS",
    "DP",
    "DR",
    "Width",
    "Min",
    "Max",
    "Nmax",
    "Nzeros",
    "Mode",
    "Mean",
    "Median",
    "Variance",
]


dataframe_orj = read_csv("./data.csv")
dataframe = dataframe_orj[feat_cols]
array = dataframe.values
# separate array into input and output components
X = array[:, 0 : len(feat_cols)]
Y = array[:, len(feat_cols) - 1]

scaler = MinMaxScaler(feature_range=(0, 1))
rescaledX = scaler.fit_transform(X)
# summarize transformed data
np.set_printoptions(precision=3)

print(rescaledX[0:5, :])
new_df = pd.DataFrame(rescaledX, columns=feat_cols)
new_df["NSP"] = dataframe_orj["NSP"]
new_df["Date"] = dataframe_orj["Date"]
new_df.to_csv("data_normalized.csv", index=False)
