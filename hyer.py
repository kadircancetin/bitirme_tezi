from pyspark.ml.feature import VectorAssembler
from pyspark.mllib.clustering import (BisectingKMeans, BisectingKMeansModel,
                                      GaussianMixture, GaussianMixtureModel,
                                      KMeans, PowerIterationClustering,
                                      PowerIterationClusteringModel,
                                      StreamingKMeans)
from pyspark.mllib.linalg import Vectors
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, desc, udf
from pyspark.sql.types import FloatType, IntegerType

import numpy as np

feat_cols = [
    "LBE",
    "LB",
    "AC",
    "FM",
    "UC",
    "ASTV",
    "MSTV",
    "ALTV",
    "MLTV",
    "DL",
    "DS",
    "DP",
    "DR",
    "Width",
    "Min",
    "Max",
    "Nmax",
    "Nzeros",
    "Mode",
    "Mean",
    "Median",
    "Variance",
    "Tendency",
    "A",
    "B",
    "C",
    "D",
    "E",
    "AD",
    "DE",
    "LD",
    "FS",
    "SUSP",
    "CLASS",
]

spark = SparkSession.builder.appName("Python Spark SQL basic example").getOrCreate()

df_ham = spark.read.csv("data.csv", header=True, sep=",", inferSchema=True)
df_ham = df_ham.where("NSP != 2")
df = df_ham.select(*feat_cols)


vectorAss = VectorAssembler(inputCols=feat_cols, outputCol="features")
df_ham = vectorAss.transform(df_ham)
data_for_kmeans = df_ham.rdd.map(
    lambda row: Vectors.dense([x for x in row["features"]])
)


k = 3
for _ in range(1):
    # CLUSTERS = KMeans.train(data_for_kmeans, k=k, maxIterations=1000)
    # input("devam?")
    CLUSTERS = BisectingKMeans.train(data_for_kmeans, k=k)

    def add_distance(*row):
        record = Vectors.dense(row[0])
        cluster = CLUSTERS.predict(record)
        centroid = CLUSTERS.centers[cluster]
        return float(np.linalg.norm(record - centroid))

    udfValueToCategory = udf(add_distance, FloatType())
    df_ham_tmp = df_ham.withColumn("distance_kMeans", udfValueToCategory("features"))

    #########
    #########
    #########
    THRESHOLD = df_ham_tmp.sort(desc("distance_kMeans")).take(177)[-1][
        "distance_kMeans"
    ]
    print(4)

    def add_kMeans_label(*row):
        if row[0] > THRESHOLD:
            return 1
        return 0

    print(5)
    udfValueToCategory = udf(add_kMeans_label, IntegerType())
    df_ham_tmp = df_ham_tmp.withColumn(
        "label_kMeans", udfValueToCategory("distance_kMeans", "NSP")
    )
    print(6)

    def puan_hesapla(row):
        nsp = row["NSP"]
        predicted = row["label_kMeans"]

        if predicted == 1 and nsp == 3:
            return (1, 0, 0, 0)
        if predicted == 1 and nsp == 1:
            return (0, 1, 0, 0)
        if predicted == 0 and nsp == 1:
            return (0, 0, 1, 0)
        if predicted == 0 and nsp == 3:
            return (0, 0, 0, 1)

    print(7)
    sonuc = df_ham_tmp.rdd.map(puan_hesapla).collect()

    TP = 0
    FP = 0
    TN = 0
    FN = 0
    for j in sonuc:
        TP += j[0]
        FP += j[1]
        TN += j[2]
        FN += j[3]

    print(f"TP:{TP}, FP:{FP}, TN:{TN}, FN:{FN}")
